+++
title = "Découvrez le DDD"
author = "Yohann"
cover = "harrycover.jpg"
tags = ["ddd"]
description = "Découvrons le DDD en s'amusant"
showFullContent = false
+++

Je vous redirige vers la vidéo de Cyrille Martraire parce que j'ai pas vraiment envie de faire un article.

C'est vachement plus simple de partager un bon contenu plutôt que de l'écrire soi-même.

Je ne recherche pas la gloire ni l'admiration.

Juste de l'amour.
